import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  ifAuthed = false;
  constructor(private authService: AuthService) {
    this.authService.authEmmiter.subscribe((res: boolean) => {
      this.ifAuthed = res;
    });

    this.ifAuthed = this.authService.getUserAuth();
  }

  onLogout() {
    this.authService.setUserAuth(false);
  }

}
