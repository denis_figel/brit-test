import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs';
import { PingService } from '../services/ping.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnDestroy {

  authSubscription: Subscription;
  ifAuthed = false;
  isPong = false;
  isSending = false;
  constructor(private authService: AuthService,
              private pingService: PingService) {
    this.authSubscription = this.authService.authEmmiter.subscribe((res: boolean) => {
      if (!res) {
        this.isPong = false;
      }
      this.ifAuthed = res;
    });

    this.ifAuthed = this.authService.getUserAuth();
  }

  // Ping server method
  onPingServer() {
    this.isSending = true;
    this.isPong = false;

    this.pingService.ping().subscribe(() => {
      this.isPong = true;

      setTimeout(() => {
        this.isPong = false;
      }, 3000);

      this.isSending = false;
    }, error => {
      console.log(error);

      this.isSending = false;
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }

}
