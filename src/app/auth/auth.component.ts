import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {
  errorDescription = '';
  isSending = false;
  constructor(private authService: AuthService) {}

  // Submit login form
  onLogin(form: NgForm): void {
    this.errorDescription = '';

    if (form.valid) {
      this.isSending = true;
      this.authService.login(form.value).subscribe((res: User) => {
          this.authService.setUserAuth(true, res);

          this.isSending = false;
      }, error => {
        if (error && error.error) {
          this.errorDescription = error.error.error_description;
        }

        this.isSending = false;
      });
    }
  }

}
