import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { AuthedGuard } from './services/authed.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: AuthComponent, canActivate: [AuthedGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
