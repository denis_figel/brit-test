import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let copiedReq = req.clone();
    if (this.authService.isAuthed) {
      copiedReq = req.clone({headers: req.headers.set('Authorization', this.authService.getToken())});
    }

    return next.handle(copiedReq).pipe(catchError(err => {
      if (err.status === 401) {
        // TODO: In this case we have to refresh token, as i do not have endpont, this is not realizes

        this.authService.logoutUser();
      }

      return throwError(err);
    }));
  }
}
