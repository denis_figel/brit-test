import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Login } from '../models/login.model';
import { User } from '../models/user.model';

@Injectable({ providedIn: 'root' })
export class AuthService {
    isAuthed = false;
    authEmmiter = new EventEmitter<boolean>();
    constructor(private http: HttpClient,
                private router: Router) {}

    login(data: Login): Observable<User> {
        const body = new HttpParams({fromObject: <any>data})
            .append('grant_type', 'password')
            .append('scope', 'offline_access openid profile')
            .append('resource', 'api://enterprise');

        return this.http.post<User>('https://id.pe.atelierclient.com/connect/token', body);
    }

    setUserAuth(isAuthed: boolean, token?: User) {
        if (!isAuthed) {
            this.logoutUser();
        } else {
            if (token.access_token !== '') {
                localStorage.setItem('token', token.access_token);

                this.router.navigate(['']);
            }
        }

        this.isAuthed = isAuthed;

        this.authEmmiter.emit(isAuthed);
    }

    logoutUser() {
        localStorage.removeItem('token');

        this.router.navigate(['']);

        return true;
    }

    getUserAuth() {
        if (localStorage.getItem('token')) {
          this.isAuthed = true;
        }

        return this.isAuthed;
    }

    getToken() {
        return 'Bearer ' + localStorage.getItem('token');
    }
}
