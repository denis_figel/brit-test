import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthedGuard implements CanActivate {
  constructor(private _router: Router,
              private _authService: AuthService) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this._authService.getUserAuth()) {
      this._router.navigate(['']);
      return false;
    } else {
      return true;
    }
  }
}
